#ifndef UTILS__H
#define UTILS__H
#include "tipos.h"

#define MAX_LINE 200

status_t imprimir_ayuda(FILE *pt_archivo);
char * fulltrim(char *s);

#endif