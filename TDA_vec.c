#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "TDA_vec.h"

st_vector vec_memoria_crear(memoria_t **vec_memoria, size_t cant_palabras){

    if(!vec_memoria)
        return ST_ERROR_PUNTERO_NULL;

    *vec_memoria = (memoria_t*)calloc(1, sizeof(memoria_t));

    if(*vec_memoria == NULL)
        return ST_ERROR_MEMORIA_VECTOR;

    (*vec_memoria)->palabra = (int*)malloc(cant_palabras * sizeof(int));

    if((*vec_memoria)->palabra == NULL){
        free((*vec_memoria) -> palabra);
        (*vec_memoria) -> palabra = NULL;
        free(*vec_memoria);
        (*vec_memoria) = NULL;

            return ST_ERROR_MEMORIA_PALABRA_ESTRUCTURA;
    }
    (*vec_memoria) -> pedido = cant_palabras;
    return ST_VEC_OK;
}

st_vector destruir_mem_vec(memoria_t **vec_memoria){

    if(!vec_memoria)
        return ST_ERROR_PUNTERO_NULL;

    if(*vec_memoria){

        if((*vec_memoria) -> palabra){
            free((*vec_memoria) -> palabra);
            (*vec_memoria) -> palabra = NULL;
        }

        free(*vec_memoria);
        *vec_memoria = NULL;
    }
        return ST_VEC_OK;

}

st_vector mem_escribir(memoria_t *vec_memoria, int posicion){

    if(!vec_memoria)
        return ST_ERROR_PUNTERO_NULL;

    if(posicion >= vec_memoria->pedido)
        return ST_ERROR_POSICION_INVALIDA;

    fprintf(stdout, "\n%d\n", (vec_memoria -> palabra[posicion]));

    return ST_VEC_OK;
}

st_vector mem_buscar_posicion_palabra(memoria_t *vec_memoria, int posicion, int **pt_palabra){

     if(!vec_memoria || !pt_palabra)
        return ST_ERROR_PUNTERO_NULL;

    if(posicion >= vec_memoria->pedido)
        return ST_ERROR_POSICION_INVALIDA;

    *pt_palabra = &(vec_memoria -> palabra[posicion]);

    return ST_VEC_OK;
}


st_vector mem_guardar(memoria_t *vec_memoria, int palabra, int posicion){

    if(!vec_memoria)
        return ST_ERROR_PUNTERO_NULL;

    if(posicion >= (vec_memoria->pedido))
        return ST_ERROR_POSICION_INVALIDA;

    vec_memoria -> palabra[posicion] = palabra;

    return ST_VEC_OK;
}









