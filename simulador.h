#ifndef SIMULADOR__H
#define SIMULADOR__H

#include "tda_lista.h"

#define MAX_LINE 200
#define CARACT_COMENT ';'
#define NUM_MAX 9999999
#define NUM_MIN 0
#define SHIFT_OPCODE 9
#define MASK_OPCODE 0xFF80
#define MASK_OPERANDO 0xFE00
#define MAX_OPCODE 127
#define MAX_OPERANDO 511
#define DIVISOR 10000
#define CANT_BYTES 2
#define CONDICION_DE_SALIDA 99999999
#define POS_OPCODE 10000
#define SALTO_FILA 10
#define ESPACIO "    "
#define NO_ASCII "."
#define ENTER "\n"


#include "tda_lista.h"
#include "mensajes.h"


typedef status_t (*pt_fn)(datos_t *);

typedef enum {OPCODE_DATO = 0,
              OP_LEER = 10, OP_ESCRIBIR,
              OP_CARGAR = 20, OP_GUARDAR,
              OP_SUMAR = 30, OP_RESTAR, OP_DIVIDIR, OP_MULTIPLICAR,
              OP_JMP = 40, OP_JMPNEG, OP_JMPZERO, OP_JNZ, OP_DJNZ, OP_HALT} opcode_t;


int opcode_pal(int palabra);
int operando_pal(int palabra);
int bin_to_dec(int palabra_bin);
retval_t destruir_dato_datos_t(datos_list_t ** pt_datos_list);
status_t crear_datos_simpletron(datos_list_t **pt_datos_list, size_t cant_palabras);
status_t leer_archivo_txt(FILE *pt_file, datos_t *pt_datos, size_t pos_guardar);
status_t leer_archivo_binario(FILE *pt_archivo, datos_t *pt_datos, size_t pos_guardar);
status_t simulador_simpletron(datos_list_t **lista_simpletron);
status_t volcado_txt(datos_list_t *lista_simpletron);
status_t volcado_bin(datos_list_t *lista_simpletron);
char * diccionario(int status);

#endif
