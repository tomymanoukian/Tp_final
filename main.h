#ifndef MAIN__H
#define MAIN__H

#if defined ENGLISH
#define ARCHIVO_AYUDA "ayuda_eng.txt"
#else
#define ARCHIVO_AYUDA "ayuda_esp.txt"
#endif

#include <limits.h>
#include "simulador.h"


#define CANT_PAL_DEFAULT 50
#define CANT_NO_ARG 1
#define CANT_PAL_MIN 1
#define CANT_PAL_MAX ULONG_MAX / 2
#define ARG_HELP "-h"
#define ARG_MEMORIA "-m"
#define ARG_FORMATO_SALIDA "-f"
#define ARG_SALIDA_TXT "txt"
#define ARG_SALIDA_BIN "bin"
#define ARG_STDIN "-"
#define CARAC_FORMATO_ENTRADA ':'
#define ARG_ENTRADA_TXT "t:"
#define ARG_ENTRADA_BIN "b:"
#define NOM_SALIDA_STDIN_TXT "dump_simpletron.txt"
#define NOM_SALIDA_STDIN_BIN "dump_simpletron.bin"
#define CARAC_EXTENSION_ARCHIVO '.'
#define STR_DUMP_TXT "_dump.txt"
#define STR_DUMP_BIN "_dump.bin"


#endif
