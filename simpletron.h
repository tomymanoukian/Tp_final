#ifndef SIMPLETRON__H
#define SIMPLETRON__H

#include "TDA_vec.h"
#include "tipos.h"

#define PAL_MAX 65535
#define PAL_MIN 0

typedef struct datos{

    memoria_t* memoria;
    size_t cant_palabras;
    size_t program_counter;
    long acumulador;
} datos_t;

status_t leer(datos_t *pt_datos);
status_t escribir(datos_t *pt_datos);
status_t cargar(datos_t *pt_datos);
status_t guardar(datos_t *pt_datos);
status_t sumar(datos_t *pt_datos);
status_t restar(datos_t *pt_datos);
status_t dividir(datos_t *pt_datos);
status_t multiplicar(datos_t *pt_datos);
status_t jmp(datos_t *pt_datos);
status_t jmpneg(datos_t *pt_datos);
status_t jmpzero(datos_t *pt_datos);
status_t jnz(datos_t *pt_datos);
status_t djnz(datos_t *pt_datos);
status_t halt(datos_t *pt_datos);

#endif
