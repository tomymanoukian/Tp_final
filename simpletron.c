#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include "simpletron.h"
#include "utils.h"
#include "simulador.h"

status_t leer(datos_t *pt_datos){
    
    char str_pal[MAX_LINE], *fin, *perr;
    long numero;
    int palabra, opcode, operando, pos_guardar, *pt_palabra = NULL;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if(!fgets(str_pal, MAX_LINE, stdin))
        return ST_ERROR_FGETS;


    if((fin = strchr(str_pal, CARACT_COMENT)))
        *fin = '\0';

    if(!fulltrim(str_pal))
        return ST_ERROR_FULTRIM;

    numero = strtol(str_pal, &perr, 10);

    if(*perr != '\0')
        return ST_ERROR_LEER_ARCHIVO;

    if(numero > NUM_MAX)
        return ST_ERROR_TAMANIO_PALABRA;

    if(numero < NUM_MIN){
        return ST_ERROR_PALABRA_INVALIDA;
    }

    if((opcode = numero / DIVISOR) > MAX_OPCODE || (operando = numero % DIVISOR) > MAX_OPERANDO){
        return ST_ERROR_PALABRA_INVALIDA;
    }

    palabra = opcode;
    palabra = (palabra << SHIFT_OPCODE);
    palabra &= MASK_OPERANDO;
    palabra |= operando;

    if(mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)/*Busca la dirección de palabra que está en la posición program_counter, y la guarda en pt_palabra*/
        return ST_ERROR_BUSCAR_PALABRA;

    pos_guardar = operando_pal(*pt_palabra);

    if(mem_guardar(pt_datos -> memoria, palabra, pos_guardar) != ST_VEC_OK)
        return ST_ERROR_GUARDAR;

    return ST_OK;
}

status_t escribir(datos_t *pt_datos){

    int operando, *pt_palabra = NULL;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)/*Busca la dirección de palabra que está en la posición program_counter, y la guarda en pt_palabra*/
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if(operando > pt_datos -> cant_palabras)
        return ST_ERROR_MEMORIA_OPERANDO_INVALIDA;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, operando, &pt_palabra) != ST_VEC_OK) /*Busca la direccion de la palabra en la posicion operando y la carga en pt_palabra*/
        return ST_ERROR_BUSCAR_PALABRA;

    fprintf(stdout, "\n%+07d\n", *pt_palabra); /*Imprime la palabra en posicion operando*/

    return ST_OK;
}

status_t cargar(datos_t *pt_datos){

    int operando, *pt_palabra = NULL;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if(operando > pt_datos -> cant_palabras)
        return ST_ERROR_MEMORIA_OPERANDO_INVALIDA;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, operando, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    pt_datos -> acumulador = *pt_palabra;

    return ST_OK;
}

status_t guardar(datos_t *pt_datos){

    int pos_guardar, *pt_palabra = NULL, palabra;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria,pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    pos_guardar = operando_pal(*pt_palabra);

    if(pos_guardar > pt_datos -> cant_palabras)
        return ST_ERROR_MEMORIA_OPERANDO_INVALIDA;

    palabra = (pt_datos->acumulador);

    if(palabra < PAL_MIN || palabra > PAL_MAX)
        return ST_ERROR_TAMANIO_ACUMULADOR;

    if(mem_guardar(pt_datos -> memoria, palabra, pos_guardar) != ST_VEC_OK)
        return ST_ERROR_GUARDAR;

    return ST_OK;
}

status_t sumar(datos_t *pt_datos){

    int operando, *pt_palabra = NULL;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

     if( mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if(operando > pt_datos -> cant_palabras)
        return ST_ERROR_MEMORIA_OPERANDO_INVALIDA;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, operando, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    if((pt_datos -> acumulador += (*pt_palabra)) > PAL_MAX || pt_datos -> acumulador < PAL_MIN)
        return ST_ERROR_SUMA_INVALIDA;

    return ST_OK;
}

status_t restar(datos_t *pt_datos){

    int operando, *pt_palabra = NULL;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria,pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if(operando > pt_datos -> cant_palabras)
        return ST_ERROR_MEMORIA_OPERANDO_INVALIDA;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, operando, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    if((pt_datos -> acumulador -= (*pt_palabra)) > PAL_MAX || pt_datos -> acumulador < PAL_MIN)
        return ST_ERROR_RESTA_INVALIDA;

    return ST_OK;
}

status_t dividir(datos_t *pt_datos){

    int *pt_palabra, operando;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if((operando) == 0)
        return ST_ERROR_DIVISION_POR_CERO;

    if((pt_datos -> acumulador /= operando) > PAL_MAX || pt_datos -> acumulador < PAL_MIN)
        return ST_ERROR_DIVISION_INVALIDA;

    return ST_OK;
}

status_t multiplicar(datos_t *pt_datos){

    int *pt_palabra, operando;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if((pt_datos -> acumulador *= operando) > PAL_MAX || pt_datos -> acumulador < PAL_MIN)
        return ST_ERROR_MULTILPICACION_INVALIDA;

    return ST_OK;
}

status_t jmp(datos_t *pt_datos){

    int *pt_palabra, operando;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if(operando > pt_datos -> cant_palabras)
        return ST_ERROR_JMP_INVALIDO;

    pt_datos -> program_counter = operando;
    --(pt_datos -> program_counter);

    return ST_OK;
}

status_t jmpneg(datos_t *pt_datos){

    int *pt_palabra, operando;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if(operando > pt_datos -> cant_palabras)
        return ST_ERROR_JMP_INVALIDO;

    if(pt_datos -> acumulador < 0){
        pt_datos -> program_counter = operando;
        --(pt_datos -> program_counter);
    }

    return ST_OK;
}

status_t jmpzero(datos_t *pt_datos){

    int *pt_palabra, operando;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if(operando > pt_datos -> cant_palabras)
        return ST_ERROR_JMP_INVALIDO;

    if(!pt_datos -> acumulador){
        pt_datos -> program_counter = operando;
        --(pt_datos -> program_counter);
    }

    return ST_OK;
}

status_t jnz(datos_t *pt_datos){

    int *pt_palabra, operando;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if(operando > pt_datos -> cant_palabras)
        return ST_ERROR_JMP_INVALIDO;

    if(pt_datos -> acumulador){
        pt_datos -> program_counter = operando;
        --(pt_datos -> program_counter);
    }

    return ST_OK;
}

status_t djnz(datos_t *pt_datos){

    int *pt_palabra, operando;

    if(!pt_datos || !pt_datos -> memoria)
        return ST_ERROR_PUNTERO_NULO;

    if( mem_buscar_posicion_palabra(pt_datos -> memoria, pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    operando = operando_pal(*pt_palabra);

    if(operando > pt_datos -> cant_palabras)
        return ST_ERROR_JMP_INVALIDO;


    if(pt_datos -> acumulador){

        --(pt_datos -> acumulador);

        pt_datos -> program_counter = operando;
        --(pt_datos -> program_counter);
    }

    return ST_OK;
}
