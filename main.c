#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"


int main (int argc, char *argv[])
{
    status_t status;
    size_t cant_palabras = CANT_PAL_DEFAULT;
    FILE *pt_archivo_entrada = NULL, *pt_archivo_ayuda = NULL;
    int i, j, cant_arg = CANT_NO_ARG;
    char *pt_caracter_formato = NULL, nomb_salida_aux[200], *pt_carac_extension = NULL;
    nodo_t *lista_datos_simpletron = NULL;
    retval_t retval;
    tipos_t tipo_entrada;
    tipos_t salida;
    datos_list_t *pt_datos_list = NULL;


    if(argc == CANT_NO_ARG){
        fprintf(stderr, "%s\n", MSJ_ERROR_NO_ARG);
        return EXIT_FAILURE;
    }

    for(i = 1; i < argc; i++){
        if(!(strcmp((argv[i]), ARG_HELP))){

            if(!(pt_archivo_ayuda = fopen(ARCHIVO_AYUDA, "rt"))){
                fprintf(stderr, "%s\n", MSJ_ERROR_ARCHIVO_AYUDA);
                return EXIT_FAILURE;
            }

            status = imprimir_ayuda(pt_archivo_ayuda);
            if(status != ST_OK){
                fprintf(stderr, "%s\n", MSJ_ERROR_IMPRIMIR_AYUDA);
                return EXIT_SUCCESS;
            }

            if(fclose(pt_archivo_ayuda)){
                fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_AYUDA);
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
    }

    for(i = 1; i < argc; i++){
        if(!(strcmp(argv[i], ARG_MEMORIA))){
            ++cant_arg;

            if(!argv[++i]){
                fprintf(stderr, "%s\n", MSJ_ERROR_CANTPAL_NO);
                return EXIT_FAILURE;
            }

            (cant_palabras) = strtoul((argv[i]), NULL, 10);
            ++cant_palabras; /*se pide memoria para una palabra mas para salvar un error de mem_guardar durante la ejecución de leer_archivo_txt*/

            if(cant_palabras > CANT_PAL_MAX || cant_palabras < CANT_PAL_MIN){
                fprintf(stderr, "%s\n", MSJ_ERROR_CANTPAL_INV);
                return EXIT_FAILURE;
            }
            ++cant_arg;
            break;
        }
    }

    for(i = 1; i < argc; i++){
        if(!(strcmp(argv[i], ARG_FORMATO_SALIDA))){
            ++cant_arg;

            if(!argv[++i]){
                fprintf(stderr, "%s\n", MSJ_ERROR_NO_FORMATO_SALIDA);
                return EXIT_FAILURE;
            }

            if(!(strcmp(argv[i], ARG_SALIDA_TXT)))
                salida = txt;
            else if(!(strcmp(argv[i], ARG_SALIDA_BIN)))
                salida = bin;
            else{
                fprintf(stderr, "%s\n", MSJ_ERROR_FORMATO_SALIDA_INV);
                return EXIT_FAILURE;
            }
            ++cant_arg;
            break;
        }
        else
            salida = txt;
    }

    if(!argv[cant_arg]){
        fprintf(stderr, "%s\n", MSJ_ERROR_NO_ARCHIVO_ENTRADA);
        return EXIT_FAILURE;
    }

    if(LISTA_crear(&lista_datos_simpletron)){
        fprintf(stderr, "%s\n", MSJ_ERROR_CREAR_LISTA);
        return EXIT_FAILURE;
    }


    if(!(strcmp(argv[cant_arg], ARG_STDIN))){

        if(argc != (cant_arg + 1)){
            fprintf(stderr, "%s\n", MSJ_ERROR_STDIN_Y_ARCHIVOS);
            retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
            if(retval == RV_ILLEGAL){
                fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
            }
            if(retval == RV_ERROR){
                fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
            }
            return EXIT_FAILURE;
        }

        status = crear_datos_simpletron(&pt_datos_list, cant_palabras); /*CORROBORAR*/

        if(status == ST_ERROR_PEDIR_MEMORIA){
            fprintf(stderr, "%s\n", MSJ_ERROR_NO_MEM);
            retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
            if(retval == RV_ILLEGAL){
                fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
            }
            if(retval == RV_ERROR){
                fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
            }
            return EXIT_FAILURE;
        }

        if(status == ST_ERROR_PUNTERO_NULO){
            fprintf(stderr, "%s\n", MSJ_ERROR_VEC_MEM_NULL);
            retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
            if(retval == RV_ILLEGAL){
                fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
            }
            if(retval == RV_ERROR){
                fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
            }
            return EXIT_FAILURE;
        }

        if(salida == txt){
            if(!(pt_datos_list -> pt_archivo_salida = fopen(NOM_SALIDA_STDIN_TXT, "wt"))){
                fprintf(stderr, "%s\n", MSJ_ERROR_ARCHIVO_SALIDA);
                retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                if(retval == RV_ILLEGAL){
                    fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                }
                if(retval == RV_ERROR){
                    fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                }
                return EXIT_FAILURE;
            }
        }

        else if(salida == bin){
            if(!(pt_datos_list -> pt_archivo_salida = fopen(NOM_SALIDA_STDIN_TXT, "wb"))){
                fprintf(stderr, "%s\n", MSJ_ERROR_ARCHIVO_SALIDA);
                return EXIT_FAILURE;
            }
        }

        for(i = 0; i < cant_palabras; i++){

            status = leer_archivo_txt(stdin, pt_datos_list -> pt_datos, i);

            if(status == ST_FIN_ARCHIVO){
                pt_datos_list -> pt_datos -> cant_palabras = --i;
                break;
            }

            if(status == ST_ERROR_PUNTERO_NULO){
                fprintf(stderr, "%s\n", MSJ_ERROR_PTR_NULO);
                retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                if(retval == RV_ILLEGAL){
                    fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                }
                if(retval == RV_ERROR){
                    fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                }
                return EXIT_FAILURE;
            }

            if(status == ST_ERROR_FGETS || status == ST_ERROR_FULTRIM || status == ST_ERROR_LEER_ARCHIVO){
                fprintf(stderr, "%s\n", MSJ_ERROR_PROCESAR_ARCHIVO);
                retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                if(retval == RV_DOUBLE_ERROR || retval == RV_ILLEGAL)
                    fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                if(retval == RV_DOUBLE_ERROR || retval == RV_ERROR)
                    fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                return EXIT_FAILURE;
            }

            if(status == ST_ERROR_TAMANIO_PALABRA){
                fprintf(stderr, "%s\n", MSJ_ST_ERROR_TAMANIO_PALABRA);
                retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                if(retval == RV_ILLEGAL){
                    fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                }
                if(retval == RV_ERROR){
                    fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                }
                return EXIT_FAILURE;
            }
        }

        retval = LISTA_insertar_al_final(&lista_datos_simpletron, pt_datos_list);

            if(retval == RV_ILLEGAL){
                fprintf(stderr, "%s\n", MSJ_ERROR_NODO_NULL);
                retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                if(retval == RV_ILLEGAL){
                    fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                }
                if(retval == RV_ERROR){
                    fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                }
                return EXIT_FAILURE;
            }

            if(retval == RV_NOSPACE){
                fprintf(stderr, "%s\n", MSJ_ERROR_NO_MEM);
                retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                if(retval == RV_ILLEGAL){
                    fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                }
                if(retval == RV_ERROR){
                    fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                }
                return EXIT_FAILURE;
            }
    }

    else{
        for(i = cant_arg; i < argc; i++){

            if(!(pt_caracter_formato = strchr(argv[i], CARAC_FORMATO_ENTRADA))){
                if(!(pt_archivo_entrada = fopen(argv[i], "rt"))){
                    fprintf(stderr, "%s" "%dº" " %s\n", MSJ_ERROR_ARCHIVO_ENTRADA_INV_1, i - cant_arg + 1, MSJ_ERROR_ARCHIVO_ENTRADA_INV_2);
                    continue;
                }
            }

            else if(!(strncmp(argv[i], ARG_ENTRADA_TXT, strlen(ARG_ENTRADA_TXT)))){
                if(!(pt_archivo_entrada = fopen(++pt_caracter_formato, "rt"))){
                    fprintf(stderr, "%s" "%dº" "%s\n", MSJ_ERROR_ARCHIVO_ENTRADA_INV_1, i - cant_arg + 1, MSJ_ERROR_ARCHIVO_ENTRADA_INV_2);
                    continue;
                }
                else{
                    tipo_entrada = txt;
                }
            }

            else if(!(strncmp(argv[i], ARG_ENTRADA_BIN, strlen(ARG_ENTRADA_BIN)))){
                if(!(pt_archivo_entrada = fopen(++pt_caracter_formato, "rt"))){
                    fprintf(stderr, "%s" "%dº" "%s\n", MSJ_ERROR_ARCHIVO_ENTRADA_INV_1, i - cant_arg + 1, MSJ_ERROR_ARCHIVO_ENTRADA_INV_2);
                    continue;
                }
                else
                    tipo_entrada = bin;
            }

            else{
                fprintf(stderr, "%s" "%dº" "%s\n", MSJ_ERROR_FORMATO_ENTRADA_INV_1, i - cant_arg + 1, MSJ_ERROR_FORMATO_ENTRADA_INV_2);
                    continue;
            }

            strcpy(nomb_salida_aux, pt_caracter_formato);
            if((pt_carac_extension = strchr(nomb_salida_aux, CARAC_EXTENSION_ARCHIVO)))
                *pt_carac_extension = '\0';

            status = crear_datos_simpletron(&pt_datos_list, cant_palabras);

            if(status == ST_OK)

            if(status == ST_ERROR_PEDIR_MEMORIA){
                fprintf(stderr, "%s\n", MSJ_ERROR_NO_MEM);
                retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                if(retval == RV_ILLEGAL){
                    fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                }
                if(retval == RV_ERROR){
                    fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                }
                return EXIT_FAILURE;
            }

            if(status == ST_ERROR_PUNTERO_NULO){
                fprintf(stderr, "%s\n", MSJ_ERROR_VEC_MEM_NULL);
                retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                if(retval == RV_ILLEGAL){
                    fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                }
                if(retval == RV_ERROR){
                    fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                }
                return EXIT_FAILURE;
            }


            if(salida == txt){
                if(!(pt_datos_list -> pt_archivo_salida = fopen(strcat(nomb_salida_aux, STR_DUMP_TXT), "wt"))){
                    fprintf(stderr, "%s\n", MSJ_ERROR_ARCHIVO_SALIDA);
                    retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                    if(retval == RV_DOUBLE_ERROR || retval == RV_ILLEGAL)
                        fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                    if(retval == RV_DOUBLE_ERROR || retval == RV_ERROR)
                        fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                    return EXIT_FAILURE;
                }
            }

            if(salida == bin){
                if(!(pt_datos_list -> pt_archivo_salida = fopen(strcat(nomb_salida_aux, STR_DUMP_BIN), "wb"))){
                    fprintf(stderr, "%s\n", MSJ_ERROR_ARCHIVO_SALIDA);
                    retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                    if(retval == RV_ILLEGAL){
                        fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                    }
                    if(retval == RV_ERROR){
                        fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                    }
                    return EXIT_FAILURE;
                }
            }

            if(tipo_entrada == bin){

                for(j = 0; j < cant_palabras; j++){

                    status = leer_archivo_binario(pt_archivo_entrada, pt_datos_list -> pt_datos, j);

                    if(status == ST_FIN_ARCHIVO){
                        pt_datos_list -> pt_datos -> cant_palabras = --j;
                        break;
                    }

                    else if(status == ST_ERROR_PUNTERO_NULO){
                        fprintf(stderr, "%s\n", MSJ_ERROR_PTR_NULO);
                        retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                        if(retval == RV_ILLEGAL){
                            fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                        }
                        if(retval == RV_ERROR){
                            fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                        }
                        return EXIT_FAILURE;
                    }

                    else if(status == ST_ERROR_LEER_ARCHIVO){

                        fprintf(stderr, "%s\n", MSJ_ERROR_PROCESAR_ARCHIVO);
                        retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                        if(retval == RV_ILLEGAL){
                            fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                        }
                        if(retval == RV_ERROR){
                            fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                        }
                        return EXIT_FAILURE;
                    }

                    else if(status == ST_ERROR_GUARDAR){

                        fprintf(stderr, "%s\n", MSJ_ERROR_MEM_GUARDAR);
                        retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                        if(retval == RV_ILLEGAL){
                            fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                        }
                        if(retval == RV_ERROR){
                            fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                        }
                        return EXIT_FAILURE;
                    }
                }
            }

            else if(tipo_entrada == txt){
                for(j = 0; j < cant_palabras; j++){

                    status = leer_archivo_txt(pt_archivo_entrada, pt_datos_list -> pt_datos, j);

                    if(status == ST_FIN_ARCHIVO){
                        pt_datos_list -> pt_datos -> cant_palabras = --j;
                        break;
                    }

                    else if(status == ST_ERROR_PUNTERO_NULO){
                        fprintf(stderr, "%s\n", MSJ_ERROR_PTR_NULO);
                        retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                        if(retval == RV_ILLEGAL){
                            fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                        }
                        if(retval == RV_ERROR){
                            fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                        }
                        return EXIT_FAILURE;
                    }

                    else if(status == ST_ERROR_FGETS || status == ST_ERROR_FULTRIM || status == ST_ERROR_LEER_ARCHIVO){

                        fprintf(stderr, "%s\n", MSJ_ERROR_PROCESAR_ARCHIVO);
                        retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                        if(retval == RV_ILLEGAL){
                            fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                        }
                        if(retval == RV_ERROR){
                            fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                        }
                        return EXIT_FAILURE;
                    }

                    else if(status == ST_ERROR_TAMANIO_PALABRA){
                        fprintf(stderr, "%s\n", MSJ_ST_ERROR_TAMANIO_PALABRA);
                        retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                        if(retval == RV_ILLEGAL){
                            fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                        }
                        if(retval == RV_ERROR){
                            fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                        }
                        return EXIT_FAILURE;
                    }

                    if(status == ST_ERROR_GUARDAR){

                        fprintf(stderr, "%s\n", MSJ_ERROR_MEM_GUARDAR);
                        retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                        if(retval == RV_ILLEGAL){
                            fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                        }
                        if(retval == RV_ERROR){
                            fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                        }
                        return EXIT_FAILURE;
                    }
                }
            }

            if(!pt_datos_list -> pt_datos -> cant_palabras){ /*si la cant_palabras sigue en cero significa que nunca se alcanzó el EOF para asignarle un valor, por lo tanto la cant_palabras pedidas no son suficientes*/
                fprintf(stderr,"%s\n", MSJ_ERROR_MEMORIA_INSUFICIENTE);
                return EXIT_FAILURE;
            }

            retval = LISTA_insertar_al_final(&lista_datos_simpletron, pt_datos_list);

            if(retval == RV_ILLEGAL){
                fprintf(stderr, "%s\n", MSJ_ERROR_NODO_NULL);
                retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                if(retval == RV_ILLEGAL){
                    fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                }
                if(retval == RV_ERROR){
                    fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                }
                return EXIT_FAILURE;
            }

            if(retval == RV_NOSPACE){
                fprintf(stderr, "%s\n", MSJ_ERROR_NO_MEM);
                retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
                if(retval == RV_ILLEGAL){
                    fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
                }
                if(retval == RV_ERROR){
                    fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
                }
                return EXIT_FAILURE;
            }
        }
    }

    if(LISTA_esta_vacia(lista_datos_simpletron)){
        fprintf(stderr, "%s\n", MSJ_ERROR_LISTA_VACIA);
        return EXIT_FAILURE;
    }

    status = LISTA_recorrer(lista_datos_simpletron, &simulador_simpletron);

    if(status){

        fprintf(stderr, "%s\n", diccionario(status));

        retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
        if(retval == RV_ILLEGAL){
            fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
        }
        if(retval == RV_ERROR){
            fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
        }
        return EXIT_FAILURE;
    }

    if(salida == txt)
        status = LISTA_imprimir(lista_datos_simpletron, &volcado_txt);

    else if(salida == bin)
        status = LISTA_imprimir(lista_datos_simpletron, &volcado_bin);

    retval = LISTA_destruir(&lista_datos_simpletron, &destruir_dato_datos_t);
    if(retval == RV_ILLEGAL){
        fprintf(stderr, "%s\n", MSJ_ERROR_DESTRUIR_LISTA);
        return EXIT_FAILURE;
    }
    if(retval == RV_ERR_FCLOSE){
        fprintf(stderr, "%s\n", MSJ_ERROR_CERRAR_ARCHIVO_SALIDA);
        return EXIT_FAILURE;
    }


    return EXIT_SUCCESS;
}
