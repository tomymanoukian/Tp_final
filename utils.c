#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "utils.h"
#include "tipos.h"

status_t imprimir_ayuda(FILE *pt_archivo){

    char line[MAX_LINE];

    if(!pt_archivo)
        return ST_ERROR_PUNTERO_NULO;

    while(!feof(pt_archivo)){
        
        if(!fgets(line, MAX_LINE, pt_archivo) && !feof(pt_archivo))
            return ST_ERROR_FGETS;

        fprintf(stdout, "%s", line);
    }
    
    return ST_OK;
}

char * fulltrim(char *s){

    char *inicio, *fin;

    if(!s)
        return NULL;

    for(inicio = s; isspace(*inicio) && *inicio != '\0'; inicio++);

    if(*inicio == '\0'){
        *s = '\0';
        return s;
    }

    for(fin = s + strlen(s) - 1; isspace(*fin) && fin != s; fin--);
    *++fin = '\0';

    return memmove(s, inicio, fin - inicio + 1);
}