#ifndef TDA_VEC__H
#define TDA_VEC__H

#include <stdio.h>
#include <stdlib.h>

#include "tipos.h"

typedef enum {ST_VEC_OK,
             ST_ERROR_PUNTERO_NULL,
             ST_ERROR_NO_ENTRADA,
             ST_ERROR_POSICION_INVALIDA,
             ST_ERROR_LEER_PALABRA,
             ST_ERROR_MEMORIA_VECTOR,
             ST_ERROR_MEMORIA_PALABRAS_VECTOR,
             ST_ERROR_MEMORIA_PALABRA_ESTRUCTURA} st_vector;

typedef struct vector_memoria{
    size_t pedido;
    int *palabra;
}memoria_t;

st_vector vec_memoria_crear(memoria_t **vec_memoria, size_t cant_palabras);
st_vector destruir_mem_vec(memoria_t **vec_memoria);
st_vector mem_leer(memoria_t *vec_memoria,int posicion, FILE *entrada);
st_vector mem_escribir(memoria_t *vec_memoria, int posicion);
st_vector mem_buscar_posicion_palabra(memoria_t *vec_memoria, int posicion, int **pt_palabra);
st_vector mem_guardar(memoria_t *vec_memoria, int palabra, int posicion);


#endif







