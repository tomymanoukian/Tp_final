#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "simulador.h"

int opcode_pal(int palabra){
    int opcode;

    opcode = (palabra >> SHIFT_OPCODE);
    opcode &= ~MASK_OPCODE;

    return opcode;
}

int operando_pal(int palabra){

    palabra &= ~MASK_OPERANDO;

    return palabra;
}

int bin_to_dec(int palabra_bin){

    int opcode, palabra_dec, operando;

    opcode = palabra_bin >> SHIFT_OPCODE;

    palabra_dec = opcode * POS_OPCODE;

    operando = palabra_bin & ~MASK_OPERANDO;

    palabra_dec += operando;

    return palabra_dec;
}

retval_t destruir_dato_datos_t(datos_list_t ** pt_datos_list){

    st_vector status_vec;
    bool_t error_fclose = false;

    if(!pt_datos_list)
        return RV_ILLEGAL;

    if(fclose((*pt_datos_list) -> pt_archivo_salida))
        error_fclose = true;

    status_vec = destruir_mem_vec(&((*pt_datos_list) -> pt_datos -> memoria));

    free((*pt_datos_list) -> pt_datos);
    (*pt_datos_list) -> pt_datos = NULL;
    free(*pt_datos_list);
    *pt_datos_list = NULL;

    if(status_vec)
        return RV_ILLEGAL;

    if(error_fclose)
        return RV_ERROR;

    return RV_SUCCESS;
}

status_t crear_datos_simpletron(datos_list_t **pt_datos_list, size_t cant_palabras){

    st_vector status_vec;

    if(!pt_datos_list)
        return ST_ERROR_PUNTERO_NULL;

    if(!(*pt_datos_list = (datos_list_t *)calloc(1, sizeof(datos_list_t))))
        return ST_ERROR_PEDIR_MEMORIA;

    if(!((*pt_datos_list) -> pt_datos = (datos_t *)calloc(1, sizeof(datos_t)))){
        free(*pt_datos_list);
        *pt_datos_list = NULL;
        return ST_ERROR_PEDIR_MEMORIA;
    }

    status_vec = vec_memoria_crear(&((*pt_datos_list) -> pt_datos -> memoria), cant_palabras);

    if(status_vec == ST_ERROR_PUNTERO_NULL){
        free((*pt_datos_list) -> pt_datos);
        (*pt_datos_list) -> pt_datos = NULL;
        free(*pt_datos_list);
        *pt_datos_list = NULL;
        return ST_ERROR_PUNTERO_NULO;
    }

    if(status_vec){
        free((*pt_datos_list) -> pt_datos);
        (*pt_datos_list) -> pt_datos = NULL;
        free(*pt_datos_list);
        *pt_datos_list = NULL;
        return ST_ERROR_PEDIR_MEMORIA;
    }

    return ST_OK;
}

status_t leer_archivo_txt(FILE *pt_file, datos_t *pt_datos, size_t pos_guardar){ /*pos_guardar: indica en que posición de la memoria se guardará la palabra. Para leer de stdin llenar file* con stdin */

    char str_pal[MAX_LINE], *fin, *perr;
    long numero;
    int palabra, opcode, operando;


    if(!pt_datos || !pt_datos -> memoria || !pt_file)
        return ST_ERROR_PUNTERO_NULO;

    if(!fgets(str_pal, MAX_LINE, pt_file)){

        if(feof(pt_file)){
            return ST_FIN_ARCHIVO;
        }
        return ST_ERROR_FGETS;
    }

    if(feof(pt_file)){
        return ST_FIN_ARCHIVO;
    }

    if((fin = strchr(str_pal, CARACT_COMENT)))
        *fin = '\0';

    if(!fulltrim(str_pal))
        return ST_ERROR_FULTRIM;


    numero = strtol(str_pal, &perr, 10);
    if(*perr != '\0')
        return ST_ERROR_LEER_ARCHIVO;

    if(numero == CONDICION_DE_SALIDA)
        return ST_FIN_ARCHIVO;

    if(numero > NUM_MAX)
        return ST_ERROR_TAMANIO_PALABRA;

    if(numero < NUM_MIN){
        return ST_ERROR_PALABRA_INVALIDA;
    }

    if((opcode = numero / DIVISOR) > MAX_OPCODE || (operando = numero % DIVISOR) > MAX_OPERANDO){
        return ST_ERROR_PALABRA_INVALIDA;
    }

    palabra = opcode;
    palabra = (palabra << SHIFT_OPCODE);
    palabra &= MASK_OPERANDO;
    palabra |= operando;


    if(mem_guardar(pt_datos -> memoria, palabra, pos_guardar) != ST_VEC_OK)
        return ST_ERROR_GUARDAR;


    return ST_OK;
}

status_t leer_archivo_binario(FILE *pt_archivo, datos_t *pt_datos, size_t pos_guardar){

    int palabra = 0;

    if(!pt_datos || !pt_datos -> memoria || !pt_archivo)
        return ST_ERROR_PUNTERO_NULO;

    if(feof(pt_archivo))
        return ST_FIN_ARCHIVO;

    if(!fread(&palabra, CANT_BYTES, 1, pt_archivo))
        return ST_ERROR_LEER_ARCHIVO;

    if(mem_guardar(pt_datos -> memoria, palabra, pos_guardar) != ST_VEC_OK)
        return ST_ERROR_GUARDAR;

    return ST_OK;
}


pt_fn switch_fn(int opcode){

    switch(opcode){

        case OP_LEER:
            return leer;

        case OP_ESCRIBIR:
            return escribir;

        case OP_CARGAR:
            return cargar;

        case OP_GUARDAR:
            return guardar;

        case OP_SUMAR:
            return sumar;

        case OP_RESTAR:
            return restar;

        case OP_DIVIDIR:
            return dividir;

        case OP_MULTIPLICAR:
            return multiplicar;

        case OP_JMP:
            return jmp;

        case OP_JMPNEG:
            return jmpneg;

        case OP_JMPZERO:
            return jmpzero;

        case OP_JNZ:
            return jnz;

        case OP_DJNZ:
            return djnz;

        default:
            return NULL;
    }
}


status_t simulador_simpletron(datos_list_t **pt_datos_list){

    status_t status;
    st_vector status_vec;
    pt_fn funcion;
    int *palabra, opcode;

    if(!pt_datos_list)
        return ST_ERROR_PUNTERO_NULO;

    for((*pt_datos_list) -> pt_datos -> program_counter = 0; (*pt_datos_list) -> pt_datos -> program_counter <= (*pt_datos_list) -> pt_datos -> cant_palabras; (*pt_datos_list) -> pt_datos -> program_counter++){

        status_vec = mem_buscar_posicion_palabra((*pt_datos_list) -> pt_datos -> memoria, (*pt_datos_list) -> pt_datos -> program_counter, &palabra);

        if(status_vec == ST_ERROR_PUNTERO_NULL)
            return ST_ERROR_PUNTERO_NULO;
        if(status_vec == ST_ERROR_POSICION_INVALIDA)
            return ST_ERROR_BUSCAR_PALABRA;

        opcode = opcode_pal(*palabra);

        if(opcode == OP_HALT)
            break;

        if(!(funcion = switch_fn(opcode)))
            return ST_ERROR_OPCODE_INVALIDO;

        status = funcion((*pt_datos_list) -> pt_datos);

        if(status)
            return status;
    }
    return ST_OK;
}

status_t volcado_txt(datos_list_t *lista_simpletron){

    int i,num_fila,j,k;
    int *pt_palabra = NULL;

    if(!lista_simpletron)
        return ST_ERROR_PUNTERO_NULO;

    fprintf(lista_simpletron -> pt_archivo_salida, "%s\n",MSJ_REGS);
    fprintf(lista_simpletron -> pt_archivo_salida, "%16s %8X\n",MSJ_ACC,(unsigned) lista_simpletron -> pt_datos -> acumulador);
    fprintf(lista_simpletron -> pt_archivo_salida, "%16s %8lu\n",MSJ_PROG, lista_simpletron -> pt_datos -> program_counter);

    if(mem_buscar_posicion_palabra(lista_simpletron -> pt_datos -> memoria, lista_simpletron -> pt_datos -> program_counter, &pt_palabra) != ST_VEC_OK)
        return ST_ERROR_BUSCAR_PALABRA;

    fprintf(lista_simpletron -> pt_archivo_salida, "%16s  %+07d\n",MSJ_INST, bin_to_dec(*pt_palabra));
    fprintf(lista_simpletron -> pt_archivo_salida, "%16s %8X\n",MSJ_OPCO,(unsigned)opcode_pal(*pt_palabra));
    fprintf(lista_simpletron -> pt_archivo_salida, "%16s %8X\n",MSJ_OPER,(unsigned)operando_pal(*pt_palabra));

    fprintf(lista_simpletron -> pt_archivo_salida, "\n");
    fprintf(lista_simpletron -> pt_archivo_salida, "%s\n",MSJ_MEM);
    fprintf(lista_simpletron -> pt_archivo_salida, "\n");

    num_fila = ((lista_simpletron -> pt_datos -> cant_palabras) - ((lista_simpletron -> pt_datos -> cant_palabras) % 10) + 10);

    for(i = 0; i < num_fila; i += SALTO_FILA){

        fprintf(lista_simpletron -> pt_archivo_salida, "%3X",i);
        for(j = i; j < (i + SALTO_FILA) ; j++){

                if(mem_buscar_posicion_palabra(lista_simpletron -> pt_datos -> memoria, j, &pt_palabra) != ST_VEC_OK)
                    return ST_ERROR_BUSCAR_PALABRA;
                if(j > lista_simpletron -> pt_datos -> cant_palabras && j < (i + SALTO_FILA))
                    fprintf(lista_simpletron -> pt_archivo_salida, "%6.4s", ESPACIO);
                else
                    fprintf(lista_simpletron -> pt_archivo_salida, "%6.4X",(unsigned)*pt_palabra);

        }

        fprintf(lista_simpletron -> pt_archivo_salida, "%6.4s", ESPACIO);

        for(k = i; k < (i+SALTO_FILA) && k <= lista_simpletron->pt_datos->cant_palabras; k++){

            if(mem_buscar_posicion_palabra(lista_simpletron -> pt_datos -> memoria, j, &pt_palabra) != ST_VEC_OK)
                    return ST_ERROR_BUSCAR_PALABRA;

            if(isprint(*pt_palabra))
                fputc(*pt_palabra, lista_simpletron -> pt_archivo_salida);
            else
                fprintf(lista_simpletron -> pt_archivo_salida,"%s", NO_ASCII);
        }
        fprintf(lista_simpletron -> pt_archivo_salida, ENTER);
    }

    return ST_OK;
}

status_t volcado_bin(datos_list_t *lista_simpletron){

    int i, *pt_palabra;

    if(!lista_simpletron){
        return ST_ERROR_PUNTERO_NULO;
    }

    for(i = 0; i < lista_simpletron -> pt_datos -> cant_palabras ; i++){

        if(mem_buscar_posicion_palabra(lista_simpletron -> pt_datos -> memoria, i, &pt_palabra) != ST_VEC_OK)
            return ST_ERROR_BUSCAR_PALABRA;

        fwrite(pt_palabra,1,CANT_BYTES, lista_simpletron -> pt_archivo_salida);
    }

    return ST_OK;
}


char * diccionario(int status){

    char *diccionario[] = {MSJ_ST_OK,
                           MSJ_ST_FIN_ARCHIVO,
                           MSJ_ST_ERROR_FGETS,
                           MSJ_ST_ERROR_FULTRIM,
                           MSJ_ST_ERROR_SUMA_INVALIDA,
                           MSJ_ST_ERROR_RESTA_INVALIDA,
                           MSJ_ST_ERROR_DIVISION_INVALIDA,
                           MSJ_ST_ERROR_MULTIPLICACION_INVALIDA,
                           MSJ_ST_ERROR_OPCODE_INVALIDO,
                           MSJ_ST_ERROR_PUNTERO_NULO,
                           MSJ_ST_ERROR_TAMANIO_PALABRA,
                           MSJ_ERROR_NO_MEM,
                           MSJ_ST_ERROR_DIVISION_POR_CERO,
                           MSJ_ST_ERROR_JMP_INVALIDO,
                           MSJ_ST_ERROR_MEMORIA_OPERANDO_INVALIDA,
                           MSJ_ST_ERROR_LEER_ARCHIVO,
                           MSJ_ST_ERROR_BUSCAR_PALABRA,
                           MSJ_ST_ERROR_GUARDAR,
                           MSJ_ST_ERROR_TAMANIO_PALABRA,
                           MSJ_ST_ERROR_ACUMULADOR};

    return diccionario[status];
}
