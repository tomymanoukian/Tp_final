/**
 * @file tda_lista_ite.c
 * @brief Contiene la implementación, 100% iterativa, de las primitivas de una
 * lista polimórfica simplemente enlazada.
 * @author PATRICIO MORENO
 * @date 2017-05-30
 * @license beerware
 * ---------------------------------------------------------------------------
 * "LA LICENCIA BEER-WARE" (Versión 42):
 * Yo escribí este archivo. Siempre y cuando usted mantenga este aviso, puede
 * hacer lo que quiera con él. Si nos encontramos algún día, y piensa que esto
 * vale la pena, me puede comprar una cerveza a cambio.        PATRICIO MORENO
 * ---------------------------------------------------------------------------
 */
#include <stdlib.h>
#include "tipos.h"
#include "tda_lista.h"

bool_t LISTA_esta_vacia(lista_t lista) { /*si la lista está vacía devuelve true*/
    return lista == NULL;
}

retval_t LISTA_crear(lista_t * plista) {
    if(plista == NULL)
        return RV_ILLEGAL;

    *plista = NULL;

    return RV_SUCCESS;
}

retval_t LISTA_crear_nodo(nodo_t ** pnodo, void * dato) {
    if(pnodo == NULL)
        return RV_ILLEGAL;

    if((*pnodo = (nodo_t *)calloc(1, sizeof(nodo_t))) == NULL) /*pide memoria para pnodo y retorna error si no hay suficiente espacio*/
        return RV_NOSPACE;

    (*pnodo)->siguiente = NULL;
    (*pnodo)->dato = dato;

    return RV_SUCCESS;
}

retval_t LISTA_destruir_nodo(nodo_t ** pnodo, retval_t (*destructor_dato)(datos_list_t **)) {

    datos_list_t *dato;

    if(pnodo == NULL)
        return RV_ILLEGAL;

    if(LISTA_esta_vacia(*pnodo)) /*si la lista está vacía no hay lista que destruir, por lo que se retorna el valor de salida exitosa*/
        return RV_SUCCESS;

    dato = (*pnodo)->dato; /*si no, se guarda en una variable auxiliar el dato*/

    (*pnodo)->siguiente = NULL;
    (*pnodo)->dato = NULL;
    free(*pnodo);
    *pnodo = NULL; /*se ponen todos los punteros que contiene el nodo a NULL y se libera la memoria del nodo, por último se le asigna NULL al nodo mismo*/

    return (destructor_dato != NULL) ? (*destructor_dato)(&dato) : RV_SUCCESS; /*si existe destructor_dato se ejecuta y se retorna lo que devuelva esta función. De lo contrario se retorna el valor de salida exitosa */
}

retval_t LISTA_destruir_primero(lista_t * plista, retval_t (*destructor_dato)(datos_list_t **)) { /*destruye el primer nodo de la lista y asigna el segundo nodo a la posición inicial*/
    nodo_t * primero;

    if(plista == NULL)
        return RV_ILLEGAL;

    if(*plista == NULL)/* es equivalente a LISTA_esta_vacia(*plista) */
        return RV_SUCCESS;

    primero = *plista; /*se asígna la dirección del primer nodo a una variable auxiliar*/
    *plista = (*plista)->siguiente; /*se reemplaza el primer nodo por el segundo*/

    return LISTA_destruir_nodo(&primero, destructor_dato); /*se llama a LISTA_destruir_nodo pasandole como nodo a destruir el primer nodo de la lista. luego se retorna lo que devuelve la misma función*/
}

retval_t LISTA_destruir(lista_t * plista, retval_t (*destructor_dato)(datos_list_t **)) { /*destruye una lista completamente*/
    nodo_t * nodo_a_borrar = NULL;
    retval_t rv;
    bool_t error_fclose = false; /*esta variable fue agregada ya que el destructor_dato debe cerrar un archivo, pero si esto falla en la mitad de la lista, abortar toda la operación impediría que se elimine el reto de la lista sin justificación alguna*/

    if(plista == NULL)
        return RV_ILLEGAL;

    while(*plista != NULL) {/* mientras la lista NO esté vacía */
        nodo_a_borrar = *plista;
        *plista = (*plista)->siguiente; /*se asigna a plista la dirección del siguiente nodo, para que en la siguiente vuelta del while se elimine este nodo y así sucesivamente hasta que el nodo siguiente sea nulo (fin de la lista)*/
        if((rv = LISTA_destruir_nodo(&nodo_a_borrar, destructor_dato)) == RV_ERROR) {
            error_fclose = true;
            continue; /*en caso de error al cerrar un archivo se le asigna a error_fclose un valor verdadero y se saltéa el return siguiente*/
        }
        if(rv != RV_SUCCESS)/*si rv indica un error distinto del del  fclose se retorna dicho error*/
            return rv;
    }

    if(error_fclose)
        return RV_ERR_FCLOSE;/*en caso de que se haya activado el indicador de error_fclose, se devuelve este error*/

    return RV_SUCCESS;
}

retval_t LISTA_insertar_al_ppio(lista_t * plista, void * dato) { /*inserta un nodo al principio de la lista y lo "rellena" con el dato*/
    nodo_t * nuevo;
    retval_t rv;

    if(plista == NULL)
        return RV_ILLEGAL;

    if((rv = LISTA_crear_nodo(&nuevo, dato)) != RV_SUCCESS) /*crea el nodo nuevo y le asigna dato*/
        return rv;

    nuevo->siguiente = *plista; /*le asigna al siguiente nodo del nodo creado el primer nodo de la lista*/
    *plista = nuevo; /*le asigna la primera posición de la lista al nuevo nodo*/

    return RV_SUCCESS;
}

retval_t LISTA_insertar_al_final(lista_t * plista, void * dato) { /*inserta un nodo al final de la lista y lo "rellene" con el dato*/
    retval_t rv;
    nodo_t * nuevo, *temp;

    if(plista == NULL)
        return RV_ILLEGAL;

    if((rv = LISTA_crear_nodo(&nuevo, dato)) != RV_SUCCESS) /*crea el nodo nuevo y le asigna dato*/
        return rv;

    if(*plista == NULL) {/* es equivalente a llamar a LISTA_esta_vacia(*plista)*/
        *plista = nuevo;/*si la lista está vacía le asigna dato como primer nodo */
    } else {
        for(temp = *plista; temp->siguiente != NULL; temp = temp->siguiente) /*recorre la lista con una variable auxiliar hasta llegar al último nodo no nulo*/
            ;
        temp->siguiente = nuevo; /*a este último nodo se le asigna como nodo siguiente el nodo nuevo*/
    }

    return RV_SUCCESS;
}

retval_t LISTA_insertar_decreciente(lista_t * plista, void * dato, int (*cmp)(void *, void *)) {
    retval_t rv;
    nodo_t * nuevo, * actual, *anterior;

    if(plista == NULL)
        return RV_ILLEGAL;

    if((rv = LISTA_crear_nodo(&nuevo, dato)) != RV_SUCCESS) /*crea un nuevo nodo y le asigna un dato*/
        return rv;

    /* caso: lista vacía o inserción al ppio */
    if(*plista == NULL || (*cmp)((*plista)->dato, dato) < 0) { /*si la lista está vacía o si cmp devuelve un valor negativo se asigna el nuevo nodo como primer nodo de la lista*/
        nuevo->siguiente = *plista;
        *plista = nuevo;
        return RV_SUCCESS;
    }

    /* otros casos: inserción al final o en puntos medios */
    for(actual = (*plista)->siguiente, anterior = *plista;
        actual != NULL;
        anterior = actual, actual = actual->siguiente) { /* recorre la lista comparando los datos con la función cmp hasta que encuentra algúno que sea menor o alcanza el final de la lista*/
        if((*cmp)(actual->dato, dato) < 0)
            break;
    }

    anterior->siguiente = nuevo;
    nuevo->siguiente = actual; /*asigna el nuevo puntero en la posición encontrada en el for*/

    return RV_SUCCESS;
}

void * LISTA_buscar(lista_t plista, void * t, int (*es_igual)(void *, void *)) { /*evalúa la funcion es_igual en todos los nodos de la lista, si esta devuelve un valor verdadero retorna el nodo donde se produjo*/
    if(plista == NULL || es_igual == NULL)
        return NULL;

    while(plista != NULL) {
        if((*es_igual)(t, plista->dato))
            return plista->dato;
        plista = plista->siguiente;
    }

    return NULL;
}

status_t LISTA_imprimir(lista_t plista, status_t (*impresor)(datos_list_t *)) { /*ejecuta la función impresor en todos los nodos de la lista*/

    status_t status;

    if(impresor != NULL) {
        while(plista != NULL) {
            status = (*impresor)(plista->dato);
            plista = plista->siguiente;

            if(status){
                return status;
            }
        }
    }

    return ST_OK;
}

/* con esta implementación de XX_recorrer, se puede imprimir también */
status_t LISTA_recorrer(lista_t plista, status_t (*funcion)(datos_list_t **)) { /*ejecuta función  en todos los nodos de la lista*/

    status_t status;
    if(funcion != NULL) {
        while(plista != NULL) {
            status =(*funcion)(&plista->dato);
            plista = plista->siguiente;

            if(status)
                return status;
        }
    }

    return ST_OK;
}

/* crea una nueva lista con los nodos de la lista original que al aplicarles el
 * filtro dan true */
retval_t LISTA_filtrar(lista_t plista, lista_t * pnueva, bool_t (*filtro)(void *)) { /*destruye los nodos que cumplan con la condicíon impuesta por filtro*/
    lista_t nueva;
    lista_t * pfinal;
    retval_t rv;

    if(plista == NULL || pnueva == NULL)
        return RV_ILLEGAL;

    if((rv = LISTA_crear(&nueva)) != RV_SUCCESS) /*crea una lista nueva*/
        return rv;

    pfinal = &nueva;
    while(plista != NULL) { /*recorre todos los nodos de la lista*/
        if((*filtro)(plista->dato) == true) {
            if((rv = LISTA_insertar_al_final(pfinal, plista->dato)) != RV_SUCCESS) {/*si filtro retorna true insrta al final de la lista apuntada por pfinal los nodos siguientes al que activo el filtro*/
                LISTA_destruir(&nueva, NULL); /*se destruye la lista apuntada por nueva*/
                return rv;
            }
            pfinal = &((*pfinal)->siguiente);
        }
        plista = plista->siguiente;
    }
    *pnueva = nueva;

    return RV_SUCCESS;
}

