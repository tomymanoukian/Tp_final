#ifndef TIPOS__H
#define TIPOS__H

#include "TDA_vec.h"

typedef enum {ST_OK,
              ST_FIN_ARCHIVO,
	          ST_ERROR_FGETS,
	          ST_ERROR_FULTRIM,
	          ST_ERROR_SUMA_INVALIDA,
	          ST_ERROR_RESTA_INVALIDA,
	          ST_ERROR_DIVISION_INVALIDA,
	          ST_ERROR_MULTILPICACION_INVALIDA,
              ST_ERROR_OPCODE_INVALIDO,
	          ST_ERROR_PUNTERO_NULO,
              ST_ERROR_TAMANIO_PALABRA,
	          ST_ERROR_PEDIR_MEMORIA,
	          ST_ERROR_DIVISION_POR_CERO,
	          ST_ERROR_JMP_INVALIDO,
	          ST_ERROR_MEMORIA_OPERANDO_INVALIDA,
	          ST_ERROR_LEER_ARCHIVO,
	          ST_ERROR_BUSCAR_PALABRA,
	          ST_ERROR_GUARDAR,
		      ST_ERROR_PALABRA_INVALIDA,
		      ST_ERROR_TAMANIO_ACUMULADOR} status_t;


typedef enum {standard,
	          std_input,
	          txt,
              bin} tipos_t;


typedef enum bool {
  false = 0,
  true = 1
} bool_t;


#endif
